# Starter

A Skiliko starter project where you just need to focus on the design

## Installation

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli).

``` bash
$ vue init gitlab:skiliko/starter my-project
$ cd my-project
# install dependencies
$ yarn
```

> Make sure to use a version of vue-cli >= 2.1 (`vue -V`).

## Usage

### Development

``` bash
# serve with hot reloading at localhost:3000
$ npm run dev
```

Go to [http://localhost:3000](http://localhost:3000)

### Production

``` bash
# build for production and launch the server
$ npm run build
$ npm start
```

### Generate

``` bash
# generate a static project
$ npm run generate
```

## What's included ?

### Pages

All pages of the application with pre-defined behavior.

| page | file | description |
| --- | --- | --- |
| `expert` | `pages/_id/_slug/index.vue` | The page to display the expert profile |
| `messenger` | `pages/messenger/index.vue` | The mailbox for the current user |
| `conversation` | `pages/messenger/_id/index.vue` | The conversation between the current user and an expert |
| `offer` | `pages/offer/_id/_slug/index.vue` | The page to display an offer |
| `page` | `pages/page/_id/_slug/index.vue` | The page to display a page |
| `resetPassword` | `pages/reset_password/_token.vue` | The page to reset the password |
| `transactions` | `pages/transactions/index.vue` | The page to list all the transactions for the current user |
| `transaction` | `pages/transactions/_id/index.vue` | The page to display a specific transaction |
| `account` | `pages/account.vue` | The page to display the account of the current user |
| `contact` | `pages/contact.vue` | The page to send a message to the platform (support) |
| `experts` | `pages/experts.vue` | Listing of all the experts of the platform (useless in mono) |
| `forgot-password` | `pages/forgot-password.vue` | To send the email to reset your password |
| `index` | `pages/index.vue` | Home page |
| `logout` | `pages/logout.vue` | Page to logout the current user |
| `platform-missing` | `pages/platform-missing.vue` | Error page in case the platform is not founded (or disabled) |
| `signin` | `pages/signin.vue` | To connect the user |
| `signup` | `pages/signup.vue` | To register a new user |

You can add or remove all the pages you want, just be aware that some links are sent in the transactional emails and may lead to an error if the page is not implemented.

Here is the list of all the links sent from the emails of the manager

- `index`
- `transactions`
- `resetPassword`

### Store

The template come with a pre-defined store based on `skiliko-vue-store`, feel free to extend it. You can find all the available actions/getters/mutations [here](https://gitlab.com/skiliko/skiliko-vue-store)


### Behavior

The template come with a full fonctional Skiliko application, all the view are connected to the store and Skiliko API, you just have to implement your design on top of it. Of course you can also change the behavior.
export default function ({ store, redirect, route }) {
  if (!store.getters['skiliko/session/isConnected']) { return }
  redirect('/')
}

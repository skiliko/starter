import { initForBrowser, initForNode, Session } from 'skiliko-sdk'

export default async ({ req, isServer, env, store, redirect, route }) => {
  if (route.name === 'platform-missing') { return }
  isServer
    ? initForNode(req, env.API_DOMAIN)
    : initForBrowser(env.API_DOMAIN)
  const platform = await store.dispatch('skiliko/platform/fetch')
  if (!platform) { return redirect('platform-missing') }
  Session.init(platform.token)
  if (Session.signedIn()) {
    await store.dispatch('skiliko/session/create', {})
  }
}

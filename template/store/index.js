import Vuex from 'vuex'
import SkilikoStore from 'skiliko-vue-store'

export default () => new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [
    SkilikoStore
  ]
})

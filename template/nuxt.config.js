module.exports = {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: '{{ name }}',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '{{ description }}' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  router: {
    /*
     * Ensure to initialize skiliko SDK
     */
    middleware: 'skiliko-sdk',
    /**
     * Rewrite some url base on files to have full filename for dev but stay consistent with exsisting
     * routes for Skiliko applications
     */
    extendRoutes (routes, resolve) {
      const processRoute = {
        'id-slug': route => ({ name: 'expert' }),
        'order-id': route => ({ name: 'order' }),
        'transactions-id': route => ({ name: 'transaction' }),
        'resetpassword-token': route => ({ name: 'resetPassword' }),
        'messenger-with': route => ({ name: 'conversation' }),
        'page-id-slug': route => ({ name: 'page', path: route.path.replace(/page/, 'p') }),
        'offer-id-slug': route => ({ name: 'offer', path: route.path.replace(/offer/, 'o') }),
        'reset-password-token': route => ({ path: route.path.replace(/reset-password/, 'reset_password') })
      }
      routes.forEach(route => {
        const process = processRoute[route.name] || (x => {})
        route = Object.assign(route, process(route))
      })
    }
  },
  env: {
    /**
     * Possibility to replace the API endpoint
     * if working with the backend in local you can use 'skiliko.dev'
     */
    API_DOMAIN: process.env.API_DOMAIN || 'skiliko.com'
  },
  loadingIndicator: {
    name: 'folding-cube'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /(components|layouts|middleware|pages|plugins|store)\/*\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: [
      'babel-polyfill',
      'skiliko-sdk',
      'skiliko-vue-store'
    ]
  }
}

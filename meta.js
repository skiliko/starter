module.exports = {
  helpers: {
    raw: function(options) {
      return options.fn(this)
    }
  },
  prompts: {
    name: {
      type: 'string',
      required: true,
      message: 'Project name'
    },
    description: {
      type: 'string',
      required: false,
      message: 'Project description',
      default: 'Skiliko project'
    },
    author: {
      type: 'string',
      message: 'Author'
    },
  },
  completeMessage: 'Congration your Skiliko project is ready'
};
